const path = require('path');
const express = require('express');
const hbs = require('hbs');

const geoCode = require('./utils/geoCode');
const forecast = require('./utils/forecast');
const { SERVER_PORT } = require('../config');

const app = express();

// path definitions
const publicDirectoryPath = path.join(__dirname, '../public');
const viewsPath = path.join(__dirname, '../templates/views');
const partialsPath = path.join(__dirname, '../templates/partials');

// setup handlebards engine and views locations
app.set('view engine', 'hbs');
app.set('views', viewsPath);
hbs.registerPartials(partialsPath);

// setup static directory
app.use(express.static(publicDirectoryPath));

app.get('/', (req, res) => {
    res.render('index', {
        pageTitle: 'Weather Services | Home',
        title: 'Weather',
        name: 'ed leonard',
    });
});

app.get('/about', (req, res) => {
    res.render('about', {
        pageTitle: 'Weather Services | About',
        title: 'About us',
        name: 'ed leonard',
        imgPath: '/img/coupon.jpg',
        imgAlt: 'My coupon',
    });
});

app.get('/help', (req, res) => {
    res.render('help', {
        pageTitle: 'Weather Services | Help',
        title: 'Help',
        name: 'ed leonard',
        message:
            'Simply type your desired location into the input and tap the search button.',
    });
});

app.get('/help/*', (req, res) => {
    res.render('404', {
        pageTitle: 'Weather Services | Help article not found',
        title: 'Ooops...',
        message: 'Oh no! Help article not found',
        name: 'ed leonard',
    });
});

app.get('/weather', (req, res) => {
    if (!req.query.address) {
        return res.status(422).send({
            error: 'location is a required field',
        });
    }

    geoCode(req.query.address, (err, { location, lat, lon } = {}) => {
        if (err) {
            return res.status(400).send({ error: err });
        }

        forecast(lat, lon, (err, data) => {
            if (err) {
                return res.status(400).send({ error: err });
            }

            res.status(200).send({
                address: location,
                forecast: `The weather is ${data.description}. The temperature is ${data.temp}, but it feels like ${data.feelslike}`,
            });
        });
    });
});

app.get('*', (req, res) => {
    res.render('404', {
        pageTitle: 'Weather Services | Page not found',
        title: 'Page not found',
        message: "Oh no! We can't find the page you're looking for.",
        name: 'ed leonard',
    });
});

app.listen(SERVER_PORT, () => {
    console.log('server running on port: ', SERVER_PORT);
});
