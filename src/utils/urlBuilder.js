const { MAPBOX_API_KEY, WEATHERSTACK_API_KEY } = require('../../config');

/**
 * constructs a url from a given address with which the mapbox
 * api can be queried.
 */
const buildMapboxUrl = (searchTerm) => {
    const baseUrl = 'https://api.mapbox.com/geocoding/v5/mapbox.places/';
    const encodedSearchTerm = encodeURIComponent(searchTerm) + '.json';
    const accessToken = `?access_token=${MAPBOX_API_KEY}`;
    const returnLimit = '&limit=1';

    // constructed url
    return `${baseUrl}${encodedSearchTerm}${accessToken}${returnLimit}`;
};

/**
 * construct a url from passed params to query the weatherstack api
 */
const buildWeatherstackUrl = (lat, lon) => {
    const baseUrl = 'http://api.weatherstack.com/current';
    const accessKey = `?access_key=${WEATHERSTACK_API_KEY}`;
    const query = `&query=${lat},${lon}`;
    //const units = '&units=c';

    //return `${baseUrl}${accessKey}${query}${units}`;
    return `${baseUrl}${accessKey}${query}`;
};

module.exports = {
    buildMapboxUrl,
    buildWeatherstackUrl,
};
