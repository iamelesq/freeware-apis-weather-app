const request = require('request');
const { buildWeatherstackUrl } = require('./urlBuilder');

/**
 * build a url from a parsed input param to query the
 * weatherstack api
 */
const forecast = (lat, lon, callback) => {
    const url = buildWeatherstackUrl(lat, lon);

    request({ url, json: true }, (err, res) => {
        if (err) {
            callback('unable to connect to weather service', undefined);
            return;
        } else if (res.body.error) {
            callback('unable to find location', undefined);
            return;
        }

        data = {
            name: res.body.location.name,
            description: res.body.current.weather_descriptions[0],
            temp: res.body.current.temperature,
            feelslike: res.body.current.feelslike,
        };

        callback(undefined, data);
    });
};

module.exports = forecast;
