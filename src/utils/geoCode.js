const request = require('request');
const { buildMapboxUrl } = require('./urlBuilder');

/**
 * build a url for the lookup service api. Validate the return
 * to snare any errors in connectivity or errors arising from
 * bad user input. Valid returns from the api are constructued
 * into a new object that is passed to the callback
 */
const geoCode = (address, callback) => {
    const url = buildMapboxUrl(address);

    request({ url, json: true }, (err, res) => {
        if (err) {
            callback('unable to connect to location services', undefined);
            return;
        } else if (res.body.features.length === 0) {
            callback('cannot find the place', undefined);
            return;
        }

        // build the subset of data to be passed to the callback
        data = {
            location: res.body.features[0].place_name,
            lat: res.body.features[0].center[1],
            lon: res.body.features[0].center[0],
        };

        callback(undefined, data);
    });
};

module.exports = geoCode;
