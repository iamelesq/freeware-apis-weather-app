# Weather Search app

Small weather search demo/teaching app for kids using [mapbox](https://www.mapbox.com/) and [weatherstack](https://weatherstack.com/) apis on free tiers to chain together a simple search demo routine.

features

-   Simple HTML template rendering
-   Simple endpoints in node

Suggested teaching steps

1. node cli/endpoints
2. static page rendering
3. add the weather/apis chaining
