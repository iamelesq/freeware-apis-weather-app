const weatherForm = document.querySelector('form');
const searchInput = document.querySelector('input');
const summary = document.querySelector('.summary');
const detail = document.querySelector('.detail');

weatherForm.addEventListener('submit', (e) => {
    e.preventDefault();
    summary.innerText = '';
    detail.innerText = '';
    const location = searchInput.value;

    fetch(`http://localhost:3000/weather?address=${location}`).then((res) => {
        summary.innerText = 'Loading...';

        res.json().then((data) => {
            if (data.error) {
                summary.innerText = data.error;
                console.log(data.error);
                return;
            }

            summary.innerText = location;
            detail.innerText = data.forecast;
        });
    });

    searchInput.value = '';
});
